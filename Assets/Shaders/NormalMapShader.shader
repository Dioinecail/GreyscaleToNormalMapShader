﻿#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced '_WorldSpaceCameraPos.w' with '1.0'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/NormalMapShader"
{
	Properties
	{
		_Color ("Color", Color) = (1, 1, 1, 1) // The color of the object
		_MainTex ("Main Texture", 2D) = "white" {} // Main Texture
		_Shininess ("Shininess", Float) = 10
		_SpecColor ("Specular color", Color) = (1, 1, 1, 1) // Highlights color
	}
	SubShader
	{
		Tags { "LightMode"="ForwardBase" }
		LOD 200

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
			};

			sampler2D _MainTex; // Greyscale texture which will be used for normal map conversion
			float4 _MainTex_ST;

			uniform float4 _LightColor0; // Info from UnityCG

			float4 _Tex_ST; // For tiling

			uniform float4 _Color; // The color of the object
			uniform float4 _SpecColor;
			uniform float _Shininess;

			v2f vert (appdata v)
			{
				v2f o;

				o.posWorld = UnityObjectToClipPos(v.vertex);
				o.normal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				return o;
			}
			
			fixed4 frag (v2f i) : COLOR {
 
				float val = tex2D(_MainTex, i.uv).r;
				float valX = tex2D(_MainTex, i.uv + float2(1.0 / 128.0, 0)).r;
				float valY = tex2D(_MainTex, i.uv + float2(0, 1.0 / 128.0)).r;
				float3 h = float3(1, 0, val - valX);
				float3 v = float3(0, 1, val - valY);
				float3 norm = cross(v, h);
								
				float3 normalDirection = normalize(norm);

				float3 viewDirection = normalize(_WorldSpaceCameraPos - i.posWorld.xyz);

				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);



				float3 ambientLighting = max(dot(normalDirection, lightDirection), 0.0); // ambientLighting

				float3 diffuseReflection = _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection)); // Diffuse component

				float viewDot = dot(normalDirection, normalize(lightDirection + viewDirection));
				float3 specularReflection = _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, viewDot), _Shininess);

				float3 color = (ambientLighting + diffuseReflection) * tex2D(_MainTex, i.uv) + specularReflection;

				return float4(color, 1.0);

			}
			ENDCG
		}
	}
}
