﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSourceSetter : MonoBehaviour {

    public Material targetMaterial;
    public Transform lightSourceTransform;

    private void Update() {
        if(lightSourceTransform != null && targetMaterial != null) {
            SetLightSourcePosition();
        }
    }

    public void SetLightSourcePosition() {
        targetMaterial.SetVector("_PointLightLocation", lightSourceTransform.position);
    }

    // Left over shader code


    //float val = tex2D(_MainTex, i.uv).r;
    //float valX = tex2D(_MainTex, i.uv + float2(1.0 / 128.0, 0)).r;
    //float valY = tex2D(_MainTex, i.uv + float2(0, 1.0 / 128.0)).r;
    //float3 h = float3(1, 0, val - valX);
    //float3 v = float3(0, 1, val - valY);
    //float3 norm = cross(h, v);

    //// Get the lightsource direction
    //float3 lightDirection = i.vertex.xyz - _PointLightLocation.xyz;

    //// Normalize the light direction and the normal
    //float3 N = normalize(norm);
    //float3 LightVectorNormalized = normalize(lightDirection);

    //// 
    //float lambertComponent = max(dot(N, -LightVectorNormalized), 0.0);
    //float3 diffuseLight = _LightColor * lambertComponent;

    //// находим средний вектор между освещением и наблюдателем
    //float3 eyeVec = -float3(i.vertex.xyz);
    //float3 R = normalize(eyeVec);
    //float3 halfwayVector = normalize(-LightVectorNormalized + R);

    //float specular = pow(max(dot(halfwayVector, N), 0.0), _Shiness);
    //float specularLight = _LightColor * _Shiness;

    //// итоговый цвет
    //float3 sumColor = diffuseLight + specularLight;

    //				return fixed4(sumColor, 1.0);

    void Shader() {
        //float3 normalDirection = normalize(norm);
        //float3 viewDirection = normalize(_WorldSpaceCameraPos - i.posWorld.xyz);

        //float3 objectToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;

        //float oneOverDistance = 1.0 / length(objectToLightSource);

        //float attenuation = lerp(1.0, oneOverDistance, 1.0); // Optimization for spotlights
        //float3 objectToCamera = _WorldSpaceCameraPos.xyz - i.posWorld.xyz * 1.0;

        //float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb; // ambientLighting
        //float3 diffuseReflection = attenuation * _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, objectToCamera)); // Diffuse component

        //float viewDot = dot(reflect(-objectToCamera, normalDirection), viewDirection);
        //float3 specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * max(0.0, viewDot) * _Shininess;

        //float3 color = (ambientLighting + diffuseReflection) * tex2D(_MainTex, i.uv) + specularReflection;
        //return float4(color, 1.0);
    }
}
